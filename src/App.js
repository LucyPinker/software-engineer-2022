/* eslint-disable no-restricted-globals */
import React, { useState } from 'react';
import './App.css';
import Table from './Components/Table';
import Header from './Components/Header';

const App = () => {
  const [employeeList, setEmployeeList] = useState([]);
  const nameRef = React.createRef();
  const emailRef = React.createRef();

  function validateName(input) {
    const regex = /^[a-zA-Z\s]*$/;
    if (input.match(regex)) {
      return true
    }
  }

  function validateEmail(emailAddress) {
    let regexEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (emailAddress.match(regexEmail)) {
      return true;
    }  }

  const handleSubmitClick = () => {
    // fill in with your logic to handle addition of names
    if (validateName(nameRef.current.value) && validateEmail(emailRef.current.value))
    setEmployeeList(employeeList => [...employeeList, {name: nameRef.current.value, email: emailRef.current.value} ]);
    else {
      alert("You have entered an invalid name or email address");
    }
    //nameRef.current.value = ""
    //emailRef.current.value = ""
  };

  return (
    <div class="container">
      <Header/>
        <h2>Onboard a new employee</h2>
          <div class="new-employee">
            <label>
              Name:
              <input id="name" ref={nameRef} type="text"  />
            </label>
            <label>
              Email:
              <input id="name" ref={emailRef} type="text" />
            </label>
              <input type="button" value="Add Employee" onClick={handleSubmitClick} class="btn-submit" />

          </div>
        <Table employees={employeeList}/>
    </div>
  );
};


export default App;
