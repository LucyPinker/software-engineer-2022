import React from 'react';
import './Table.css';

const Table = ({ employees}) => {
  //console.log('names:', names);

  return (
    <>
      <h2>Current Employees</h2>
        <table>
          <thead>
            <tr>
              <th>Employee Name</th>
              <th>Email Address</th>
            </tr>
          </thead>
          <tbody>
            {employees.map((employee) => (
              <tr key={employee.id}>
                <td>{employee.name}</td>
                <td>{employee.email}</td>
              </tr>
            ))}
              <tr>
                <td>Rose</td>
                <td>rose@email.com</td>
              </tr>
              <tr>
                <td>Mark</td>
                <td>mark@email.com</td>
              </tr>
          </tbody>
        </table>
    </>
  )
};

export default Table;
